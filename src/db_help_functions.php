<?php
function insert_loctaion($db, $form_data){
    try{
        $stmt = $db->prepare("INSERT INTO location(city, street_name, street_number, zip) VALUES 
                             (:city, :street_name, :street_number, :zip)");
        $stmt->bindValue(":city", !empty($form_data['city']) ? $form_data['city'] : null);
        $stmt->bindValue(":street_number", !empty($form_data['street_number']) ? $form_data['street_number'] : null);
        $stmt->bindValue(":street_name", !empty($form_data['street_name']) ? $form_data['street_name'] : null);
        $stmt->bindValue(":zip", !empty($form_data['zip']) ? $form_data['zip'] : null);
        $stmt->execute();
        return $db->lastInsertId('location_id_location_seq');
    } catch(PDOException $e){
        $this->logger->info('Error message: '. $e->getMessage());
    }
}