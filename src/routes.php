<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
include 'db_help_functions.php';

$app->get('/', function (Request $request, Response $response, $args) {
    // Render index view
    return $this->view->render($response, 'index.latte');
})->setName('index');

$app->post('/test', function (Request $request, Response $response, $args) {
    //read POST data
    $input = $request->getParsedBody();

    //log
    $this->logger->info('Your name: ' . $input['person']);

    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('redir');

$app->get('/persons', function (Request $request, Response $response, $args) {
    // query request
    /** Either show all persons, or if redirected here from find_person, show only ones satisfying condition */
    $user_data = $request->getQueryParams();
    if(empty($user_data)){
        $query_result = $this->db->query('SELECT p.*, count(c.*) as contact_count
                                          FROM person p 
                                          LEFT OUTER JOIN contact c ON p.id_person =  c.id_person 
                                          GROUP BY p.id_person ORDER BY p.id_person');
        $tpl_vars['people'] = $query_result->fetchAll();
    } else {
        $last_name = $user_data['person'];
        $query_result = $this->db->prepare('SELECT *,
                                                    (SELECT count(*) as contact_count 
                                                    FROM contact c WHERE c.id_person = p.id_person) 
                                            from person p WHERE last_name = :last_name  ORDER BY first_name');
        $query_result->bindValue(":last_name", $last_name);
        $query_result->execute();
        $tpl_vars['people'] = $query_result->fetchAll();
    }
    /**
     * Find amount of contacts and put it as a number of a button
     */
    // transfer class to [][]

    return $this->view->render($response, 'persons.latte', $tpl_vars);
})->setName('persons');

/*
 * Formular to open new data
 */
$app->get('/persons/new', function (Request $request, Response $response, $args) {
    $tpl_vars['formData'] = [
        'first_name'=>'',
        'last_name'=>'',
        'nickname'=>'',
        'id_location'=>null,
        'gender'=>'',
        'height'=>'',
        'birth_day'=>'',
        'city'=>'',
        'street_name'=>'',
        'street_number'=>'',
        'zip'=>'',
    ];
    return $this->view->render($response, 'new_person.latte', $tpl_vars);
})->setName('new_person');

/**
 * Receive data from formulator
 */

$app->post('/persons/new', function (Request $request, Response $response, $args) {
    $user_data = $request->getParsedBody();
    if(empty($user_data['first_name']) || empty($user_data['last_name']) || empty($user_data['nickname'])){
        $tpl_vars['message'] = 'Fill required fields';
    }else{
        $id_location=null;
        if(!empty($user_data['city']) || !empty($user_data['street_name']) || !empty($user_data['street_number']) || !empty($user_data['zip'])){
            $id_location = insert_loctaion($this->db, $user_data);
        }

        try{
            $statement = $this->db->prepare("INSERT INTO person(nickname, first_name, last_name, id_location, height,birth_day, gender)
                                               VALUES(:nickname, :first_name, :last_name, :id_location, :height, :birth_day, :gender)");
            $statement->bindValue(":nickname", $user_data['nickname']);
            $statement->bindValue(":first_name", $user_data['first_name']);
            $statement->bindValue(":last_name", $user_data['last_name']);
            $statement->bindValue(":id_location", $id_location);
            $statement->bindValue(":height", !empty($user_data['height']) ? $user_data['height'] : null);
            $statement->bindValue(":birth_day", !empty($user_data['birth_day']) ? $user_data['birth_day'] : null);
            $statement->bindValue(":gender", !empty($user_data['gender']) ? $user_data['gender'] : null);

            $statement->execute();
            $tpl_vars['message'] = 'Person was succesfully inserted into databse';
        }catch(PDOException $e){
            $tpl_vars['message'] = "sorry, something happened :(";
            $this->logger->info('Error message: '. $e->getMessage());
        }
    }
    $tpl_vars['formData'] = $user_data;
    return $this->view->render($response, 'new_person.latte', $tpl_vars);
})->setName('new_person');

/**
 * edit person
 */

$app->get('/persons/edit', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_person'])) {
        exit('id of the person is missing');
    } else{
        $statement = $this->db->prepare("SELECT * FROM person LEFT JOIN location ON (person.id_location = location.id_location) 
                                         WHERE id_person = :id_person");
        $statement->bindValue(":id_person", $params['id_person']);
        $statement->execute();
        $tpl_vars['formData'] = $statement->fetch();
        if (empty($tpl_vars['formData'])){
            exit('Person not found');
        } else {
            return $this->view->render($response, 'edit_person.latte', $tpl_vars);
        }
    }
})->setName('edit_person');

$app->post('/persons/edit', function (Request $request, Response $response, $args) {
    $user_data = $request->getParsedBody();
    $params = $request->getQueryParams();
    $id_person = $params['id_person'];
    if (empty($user_data['first_name']) || empty($user_data['last_name']) || empty($user_data['nickname'])) {
        $tpl_vars['message'] = 'You may not remove name or nickname!';
    } else {
        $id_location = null;
        if(!empty($user_data['city']) || !empty($user_data['street_name']) || !empty($user_data['street_number']) || !empty($user_data['zip'])) {
            if (empty($formData['id_location'])) {
                $id_location = insert_loctaion($this->db, $user_data);
            } else {
                try {
                    $stmt = $this->db->prepare("UPDATE location
                                            SET city = :city, street_name = :street_name, 
                                                street_number = :street_number, zip = :zip
                                            WHERE id_location = (SELECT id_location FROM person WHERE id_person = $id_person)");
                    $stmt->bindValue(":city", !empty($form_data['city']) ? $form_data['city'] : null);
                    $stmt->bindValue(":street_number", !empty($form_data['street_number']) ? $form_data['street_number'] : null);
                    $stmt->bindValue(":street_name", !empty($form_data['street_name']) ? $form_data['street_name'] : null);
                    $stmt->bindValue(":zip", !empty($form_data['zip']) ? $form_data['zip'] : null);
                    $stmt->execute();
                    $id_location = $this->db->lastInsertId('location_id_location_seq');
                } catch (PDOException $e) {
                    $this->logger->info('Error message: ' . $e->getMessage());
                }
            }
        }

        try{
            $statement = $this->db->prepare("UPDATE person 
                                             SET nickname = :nickname, first_name = :first_name, 
                                                 last_name = :last_name, id_location = :id_location, birth_day = :birth_day,
                                                 height = :height, gender = :gender
                                             WHERE id_person={$id_person}");
            $statement->bindValue(":nickname", $user_data['nickname']);
            $statement->bindValue(":first_name", $user_data['first_name']);
            $statement->bindValue(":last_name", $user_data['last_name']);
            $statement->bindValue(":id_location", $id_location);
            $statement->bindValue(":height", !empty($user_data['height']) ? $user_data['height'] : null);
            $statement->bindValue(":birth_day", !empty($user_data['birth_day']) ? $user_data['birth_day'] : null);
            $statement->bindValue(":gender", !empty($user_data['gender']) ? $user_data['gender'] : null);

            $statement->execute();
            $tpl_vars['message'] = 'Person was succesfully edited';
        }catch(PDOException $e){
            $tpl_vars['message'] = "sorry, something happened :(";
            $this->logger->info('Error message: '. $e->getMessage());
        }
    }
    $tpl_vars['formData'] = $user_data;
    return $this->view->render($response, 'edit_person.latte', $tpl_vars);

})->setName('edit_person');

$app->get('/persons/info', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_person'])) {
        exit('id of the person is missing');
    }
    else{
        $statement = $this->db->prepare("SELECT * FROM person LEFT JOIN location ON (person.id_location = location.id_location) 
                                         WHERE id_person = :id_person");
        $statement->bindValue(":id_person", $params['id_person']);
        $statement->execute();
        $tpl_vars['formData'] = $statement->fetch();
        if (empty($tpl_vars['formData'])){
            exit('Person not found');
        } else {
            return $this->view->render($response, 'person_info.latte', $tpl_vars);
        }
    }
})->setName('person_info');

$app->post('/persons/delete', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_person'])) {
        exit('id of the person is missing');
    }
    else{
        try {
            $statement = $this->db->prepare("DELETE FROM person WHERE id_person = :id_person");
            $statement->bindValue(":id_person", $params['id_person']);
            $statement->execute();
        }catch (PDOException $e){
            $this->logger->info($e);
        }
        return $response->withHeader('Location', $this->router->pathFor('persons'));

    }
})->setName('delete_person');

$app->get('/persons/contact-info', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_person'])) {
        exit('id of the person is missing');
    }
    else{
        try{
            $statement = $this->db->prepare("SELECT name,contact,id_contact FROM contact c
                                         LEFT JOIN contact_type ct ON c.id_contact_type = ct.id_contact_type
                                         WHERE c.id_person = :id_person GROUP BY c.id_contact, ct.id_contact_type");
            $statement->bindValue(":id_person", $params['id_person']);
            $statement->execute();
        }catch (PDOException $e){
            $this->logger->info($e);
        }
        $tpl_vars['id_person'] = $params['id_person'];
        $tpl_vars['contacts'] = $statement->fetchAll();
        return $this->view->render($response, 'contact_info.latte', $tpl_vars);

    }
})->setName('contact_info');

$app->post('/persons/contact_info/delete', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_contact'])) {
        exit('id of the contact is missing');
    }
    else{
        try {
            $statement = $this->db->prepare("DELETE FROM contact WHERE id_contact = :id_contact");
            $statement->bindValue(":id_contact", $params['id_contact']);
            $statement->execute();
        }catch (PDOException $e){
            $this->logger->info($e);
        }
        return $response->withHeader('Location', ($this->router->pathFor('contact_info'))."?id_person=".$params['id_person']);

    }
})->setName('delete_contact');

$app->get('/persons/new_contact', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_person'])) {
        exit('id of the person is missing');
    }
    else {
        try {
            $statement = $this->db->prepare("SELECT name FROM contact_type");
            $statement->execute();
        } catch (PDOException $e) {
            $this->logger->info($e);
        }
        $tpl_vars['contact'] = $statement->fetchAll();
    }
    return $this->view->render($response, 'new_contact.latte', $tpl_vars);
})->setName('new_contact');

$app->post('/persons/new_contact', function (Request $request, Response $response, $args) {
    $user_data = $request->getParsedBody();
    $params = $request->getQueryParams();
    if (empty($params['id_person'])) {
        exit('id of the person is missing');
    }
    else {
        try {
            $statement = $this->db->prepare("INSERT INTO contact(id_person, id_contact_type, contact)
                                             VALUES (:id_person, :id_contact_type, :contact)");
            $statement->bindValue(":id_person", $params['id_person']);
            $statement->bindValue(":contact", $user_data['contact']);
            $statement->bindValue(":id_contact_type", $user_data['contact_type']);
            $statement->execute();
        } catch (PDOException $e) {
            $this->logger->info($e);
        }
    }
    return $response->withHeader('Location', ($this->router->pathFor('contact_info'))."?id_person=".$params['id_person']);
})->setName('new_contact');


/**
 * Meeting routes
 */


$app->get('/meetings', function (Request $request, Response $response, $args) {
    // query request
    /** Either show all persons, or if redirected here from find_person, show only ones satisfying condition */
    $user_data = $request->getQueryParams();
    try{
        $query_result = $this->db->query('SELECT *, 
                                            (SELECT count(id_person) as participants_count 
                                             FROM person_meeting pm WHERE pm.id_meeting = m.id_meeting )
                                         FROM meeting m ORDER BY start');
        $query_result->execute();
        $tpl_vars['meetings'] = $query_result->fetchAll();
    } catch (PDOException $e){
        $this->logger->info($e);
    }
    /**
     * Find amount of contacts and put it as a number of a button
     */
    // transfer class to [][]

    return $this->view->render($response, 'meetings.latte', $tpl_vars);
})->setName('meetings');

$app->get('/meetings/participant_info', function (Request $request, Response $response, $args) {
    $user_data = $request->getParsedBody();
    $params = $request->getQueryParams();
    try {
        $statement = $this->db->prepare("SELECT first_name, last_name, person.id_person as id_person 
                                         FROM person INNER JOIN person_meeting 
                                         on person.id_person = person_meeting.id_person
                                         WHERE person_meeting.id_meeting = :id_meeting");
        $statement->bindValue(":id_meeting", $params['id_meeting']);
        $statement->execute();
    } catch (PDOException $e) {
        $this->logger->info($e);
    }
    $tpl_vars['names'] = $statement->fetchAll();
    $tpl_vars['id_meeting'] = $params['id_meeting'];
    return $this->view->render($response, 'participant_info.latte', $tpl_vars);
})->setName('participant_info');

$app->get('/meetings/participant_info/add', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    /*Would be nice to add search menu here.*/
    try {
        $statement = $this->db->prepare("SELECT first_name, last_name, id_person 
                                     FROM person 
                                     ORDER BY last_name");
        $statement->execute();
    }catch(PDOException $e){
        $this->logger->info($e);
    }
    $tpl_vars['people'] = $statement->fetchAll();
    $tpl_vars['id_meeting'] = $params['id_meeting'];
    return $this->view->render($response, 'new_participant.latte', $tpl_vars);

})->setName('add_participant');

$app->post('/meetings/participant_info/add', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    try {
        $statement = $this->db->prepare("INSERT INTO person_meeting(id_person, id_meeting)
                                         VALUES (:id_person, :id_meeting)");
        $statement->bindValue(":id_person", $params['id_person']);
        $statement->bindValue(":id_meeting", $params['id_meeting']);
        $statement->execute();
    }catch(PDOException $e){
        $this->logger->info($e);
    }
    return $response->withHeader('Location', ($this->router->pathFor('participant_info'))."?id_meeting=".$params['id_meeting']);

})->setName('add_participant');

$app->get('/meetings/edit', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_meeting'])) {
        exit('id of the meeting is missing');
    } else{
        $statement = $this->db->prepare("SELECT * FROM meeting LEFT JOIN location ON (meeting.id_location = location.id_location) 
                                         WHERE id_meeting = :id_meeting");
        $statement->bindValue(":id_meeting", $params['id_meeting']);
        $statement->execute();
        $tpl_vars['formData'] = $statement->fetch();
        if (empty($tpl_vars['formData'])){
            exit('Meeting not found');
        } else {
            return $this->view->render($response, 'edit_meeting.latte', $tpl_vars);

        }
    }
})->setName('edit_meeting');

$app->post('/meetings/edit', function (Request $request, Response $response, $args) {
    $user_data = $request->getParsedBody();
    $params = $request->getQueryParams();
    $id_meeting = $params['id_meeting'];
    if (empty($user_data['start']) || empty($user_data['duration']) || empty($user_data['description'])) {
        $tpl_vars['message'] = 'Start duration and description can t be empty!!';
    } else {
        $id_location = null;
        if(!empty($user_data['city']) || !empty($user_data['street_name']) || !empty($user_data['street_number']) || !empty($user_data['zip'])) {
            if (empty($formData['id_location'])) {
                $id_location = insert_loctaion($this->db, $user_data);
            } else {
                try {
                    $stmt = $this->db->prepare("UPDATE location
                                            SET city = :city, street_name = :street_name, 
                                                street_number = :street_number, zip = :zip
                                            WHERE id_location = (SELECT id_location FROM meeting WHERE id_meeting = {$id_meeting})");
                    $stmt->bindValue(":city", !empty($form_data['city']) ? $form_data['city'] : null);
                    $stmt->bindValue(":street_number", !empty($form_data['street_number']) ? $form_data['street_number'] : null);
                    $stmt->bindValue(":street_name", !empty($form_data['street_name']) ? $form_data['street_name'] : null);
                    $stmt->bindValue(":zip", !empty($form_data['zip']) ? $form_data['zip'] : null);
                    $stmt->execute();
                    $id_location = $this->db->lastInsertId('location_id_location_seq');
                } catch (PDOException $e) {
                    $this->logger->info('Error message: ' . $e->getMessage());
                }
            }
        }

        try{
            $statement = $this->db->prepare("UPDATE meeting 
                                             SET start = :start, duration = :duration, 
                                                 description = :description, id_location = :id_location
                                             WHERE id_meeting={$id_meeting}");
            $statement->bindValue(":start", $user_data['start']);
            $statement->bindValue(":duration", $user_data['duration']);
            $statement->bindValue(":description", $user_data['description']);
            $statement->bindValue(":id_location", $id_location);
            $statement->execute();
            $tpl_vars['message'] = 'Meeting was succesfully edited';
        }catch(PDOException $e){
            $tpl_vars['message'] = "sorry, something happened :(";
            $this->logger->info('Error message: '. $e->getMessage());
        }
    }
    $tpl_vars['formData'] = $user_data;
    return $response->withHeader('Location', ($this->router->pathFor('edit_meeting'))."?id_meeting=".$params['id_meeting']);
})->setName('edit_meeting');

$app->get('/meetings/info', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_meeting'])) {
        exit('id of the person is missing');
    }
    else {
        $statement = $this->db->prepare("SELECT * FROM meeting LEFT JOIN location ON (meeting.id_location = location.id_location) 
                                         WHERE id_meeting = :id_meeting");
        $statement->bindValue(":id_meeting", $params['id_meeting']);
        $statement->execute();
        $tpl_vars['formData'] = $statement->fetch();
        if (empty($tpl_vars['formData'])) {
            exit('Person not found');
        } else {
            return $this->view->render($response, 'meeting_info.latte', $tpl_vars);
        }
    }
})->setName('meeting_info');

$app->post('/meetings/delete', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    try {
        $statement = $this->db->prepare("DELETE FROM meeting WHERE id_meeting=:id_meeting");
        $statement->bindValue(":id_meeting", $params['id_meeting']);
        $statement->execute();
    }catch(PDOException $e){
        $this->logger->info($e);
    }

    return $response->withHeader('Location', $this->router->pathFor('meetings'));
})->setName('delete_meeting');
$app->post('/meetings/delete_participant', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    try {
        $statement = $this->db->prepare("DELETE FROM person_meeting WHERE id_person=:id_person AND id_meeting=:id_meeting");
        $statement->bindValue(":id_person", $params['id_person']);
        $statement->bindValue(":id_meeting", $params['id_meeting']);
        $statement->execute();
    }catch(PDOException $e){
        $this->logger->info($e);
    }
    return $response->withHeader('Location', ($this->router->pathFor('participant_info'))."?id_meeting=".$params['id_meeting']);
})->setName('delete_participant');

$app->get('/meetings/new', function (Request $request, Response $response, $args) {
    $tpl_vars['formData'] = [
        'start'=>'',
        'description'=>'',
        'duration'=>'',
        'id_location'=>null,
        'city'=>'',
        'street_name'=>'',
        'street_number'=>'',
        'zip'=>'',
    ];
    return $this->view->render($response, 'new_meeting.latte', $tpl_vars);
})->setName('new_meeting');

$app->post('/meetings/new', function (Request $request, Response $response, $args) {
    $user_data = $request->getParsedBody();
    if(empty($user_data['start']) || empty($user_data['duration']) || empty($user_data['description'])){
        $tpl_vars['message'] = 'Fill required fields';
    }else{
        $id_location=null;
        if(!empty($user_data['city']) || !empty($user_data['street_name']) || !empty($user_data['street_number']) || !empty($user_data['zip'])){
            $id_location = insert_loctaion($this->db, $user_data);
        }
        try{
            $statement = $this->db->prepare("INSERT INTO meeting(start, description, duration, id_location)
                                               VALUES(:start, :description, :duration, :id_location)");
            $statement->bindValue(":start", $user_data['start']);
            $statement->bindValue(":description", $user_data['description']);
            $statement->bindValue(":duration", $user_data['duration']);
            $statement->bindValue(":id_location", $id_location);
            $statement->execute();
            $tpl_vars['message'] = 'Meeting was succesfully inserted into databse';
        }catch(PDOException $e){
            $tpl_vars['message'] = "sorry, something happened :(";
            $this->logger->info('Error message: '. $e->getMessage());
        }
    }
    $tpl_vars['formData'] = $user_data;
    return $this->view->render($response, 'new_meeting.latte', $tpl_vars);
})->setName('new_meeting');

$app->get('/relations', function (Request $request, Response $response, $args) {
    $user_data = $request->getQueryParams();
    try{
        $query_result = $this->db->query('SELECT * FROM relation r
                                          INNER JOIN (SELECT id_person as id_person1, first_name as first_name_id1, last_name as last_name_id1 FROM person) 
                                                      as person1 ON r.id_person1 = person1.id_person1
                                          INNER JOIN (SELECT id_person as id_person2, first_name as first_name_id2, last_name as last_name_id2 FROM person)
                                                      as person2 ON r.id_person2 = person2.id_person2
                                          INNER JOIN relation_type rt on r.id_relation_type = rt.id_relation_type
                                          ORDER BY id_relation DESC');
        $query_result->execute();
        $tpl_vars['relations'] = $query_result->fetchAll();
    } catch (PDOException $e){
        $this->logger->info($e);
    }

    return $this->view->render($response, 'relations.latte', $tpl_vars);
})->setName('relations');

$app->get('/relations/info', function (Request $request, Response $response, $args) {
    $user_data = $request->getQueryParams();
    try{
        $query_result = $this->db->query("SELECT * FROM relation r
                                          INNER JOIN (SELECT id_person as id_person1, first_name as first_name_id1, last_name as last_name_id1 FROM person) 
                                                      as person1 ON r.id_person1 = person1.id_person1
                                          INNER JOIN (SELECT id_person as id_person2, first_name as first_name_id2, last_name as last_name_id2 FROM person)
                                                      as person2 ON r.id_person2 = person2.id_person2
                                          INNER JOIN relation_type rt on r.id_relation_type = rt.id_relation_type
                                          WHERE r.id_relation = {$user_data['id_relation']} ");
        $query_result->execute();
        $tpl_vars['relations'] = $query_result->fetchAll();
    } catch (PDOException $e){
        $this->logger->info($e);
    }
    return $this->view->render($response, 'relation_info.latte', $tpl_vars);
})->setName('relation_info');

$app->get('/relations/new', function (Request $request, Response $response, $args) {
    $tpl_vars['current'] = [
        'id_person1'=>'',
        'id_person2'=>'',
        'id_relation_type'=>null,
        'description'=>''
    ];
    try{
        $statement = $this->db->prepare("SELECT * FROM person");
        $statement->execute();
        $tpl_vars['person1'] = $statement->fetchAll();
        $tpl_vars['person2'] = $tpl_vars['person1'];
        $stmt = $this->db->prepare("SELECT * FROM relation_type");
        $stmt->execute();
        $tpl_vars['relation_list'] = $stmt->fetchAll();
    }catch(PDOException $e){
        $this->logger->info($e);
    }
    return $this->view->render($response, 'new_relation.latte', $tpl_vars);

})->setName('new_relation');

$app->post('/relations/new', function (Request $request, Response $response, $args) {
    $user_data = $request->getParsedBody();
    $params = $request->getQueryParams();
    try {
        $statement = $this->db->prepare("INSERT INTO relation(id_person1, id_person2, description, id_relation_type)
                                         VALUES (:id_person1, :id_person2, :description, :id_relation_type)");
        $statement->bindValue(":id_person1", $user_data['first_person']);
        $statement->bindValue(":id_person2", $user_data['second_person']);
        $statement->bindValue(":description", $user_data['description']);
        $statement->bindValue(":id_relation_type", $user_data['relation_type']);
        $statement->execute();
    }catch(PDOException $e){
        $this->logger->info($e);
        $tpl_vars['message'] = 'Sth happened :(';
    }
    $tpl_vars['message'] = 'Relation was successfully added';
    return $response->withHeader('Location', $this->router->pathFor('relations'));

})->setName('new_relation');

$app->post('/relation/delete', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    try {
        $statement = $this->db->prepare("DELETE FROM relation WHERE id_relation=:id_relation");
        $statement->bindValue(":id_relation", $params['id_relation']);
        $statement->execute();
    }catch(PDOException $e){
        $this->logger->info($e);
    }

    return $response->withHeader('Location', $this->router->pathFor('relations'));
})->setName('delete_relation');

$app->get('/relation/edit', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_relation'])) {
        exit('id of the relation is missing');
    } else{
        try{
            $statement = $this->db->query("SELECT *
                                           FROM relation r
                                           INNER JOIN relation_type rt on r.id_relation_type = rt.id_relation_type
                                           WHERE id_relation={$params['id_relation']}");
            $statement->execute();
            $tpl_vars['current'] = $statement->fetchAll();
            $statement = $this->db->query("SELECT * 
                                           FROM person ");
            $statement->execute();
            $tpl_vars['person1'] = $statement->fetchAll();
            $tpl_vars['person2'] = $tpl_vars['person1'];
            $statement = $this->db->query("SELECT * 
                                           FROM relation_type ");
            $statement->execute();
            $tpl_vars['relation_list'] = $statement->fetchAll();
        }catch(PDOException $e){
            $this->logger->info($e);
        }
        return $this->view->render($response, 'edit_relation.latte', $tpl_vars);

    }
})->setName('edit_relation');

$app->post('/relation/edit', function (Request $request, Response $response, $args) {
    $user_data = $request->getParsedBody();
    $params = $request->getQueryParams();
    if (empty($params['id_relation'])) {
        exit('id of the relation is missing');
    } else {
        try {
            $stmt = $this->db->prepare("UPDATE relation 
                                        SET id_person1 = :id_person1, id_person2 = :id_person2, 
                                            description = :description, id_relation_type = :id_relation_type
                                        WHERE id_relation = :id_relation");
            $stmt->bindValue(":id_person1", $user_data['first_person']);
            $stmt->bindValue(":id_person2", $user_data['second_person']);
            $stmt->bindValue(":id_relation_type", $user_data['relation_type']);
            $stmt->bindValue(":description", $user_data['description']);
            $stmt->bindValue(":id_relation", $params['id_relation']);
            $stmt->execute();
        } catch (PDOException $e) {
            $tpl_vars['message'] = 'something went wrong. :(';
            $this->logger->info($e);
        }
    }
    $tpl_vars['message'] = 'Relation edited!';
    /*
     * Add info to tpl_vars for correct
     */
    return $response->withHeader('Location', ($this->router->pathFor('edit_relation')).'?id_relation='.$params['id_relation']);

})->setName('edit_relation');
